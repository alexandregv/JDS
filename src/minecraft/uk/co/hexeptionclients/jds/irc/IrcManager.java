package uk.co.hexeptionclients.jds.irc;

import java.io.IOException;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.PircBot;

import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public class IrcManager extends PircBot {
	
	private final String IRC_HostName = "irc.freenode.net";
	private final int IRC_HostPort = 6667;
	private final String IRC_ChannelName = "#Innocent";
	
	private static String username;
	
	public IrcManager(String username) {

		this.username = username;
	}
	
	public void connect(){
		this.setAutoNickChange(true);
		this.setName(username);
		this.changeNick(username);
		JDS.theClient.clientLogger.info("[IRC] Connecting");
		Wrapper.getInstance().addIRCMessage("Attempting to connect to IRC.");
		
		try {
			this.connect(IRC_HostName, IRC_HostPort);
		} catch (NickAlreadyInUseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IrcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JDS.theClient.clientLogger.info("Joing Room");
		Wrapper.getInstance().addIRCMessage("Attempting to join.");
		this.joinChannel(IRC_ChannelName);
		JDS.theClient.clientLogger.info("Logged In");
		Wrapper.getInstance().addIRCMessage("Connected.");
	}
	
}
