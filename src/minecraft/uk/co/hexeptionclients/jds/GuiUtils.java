package uk.co.hexeptionclients.jds;

import net.minecraft.client.gui.Gui;

public class GuiUtils {
	
	public static void drawBorderRect(float left, float top, float right, float bottom, int bcolor,int color, float f) {
		Gui.drawRect(left + f, top + f, right - f, bottom - f, color);
		Gui.drawRect(left, top, left + f, bottom, bcolor);
		Gui.drawRect(left + f, top, right, top + f, bcolor);
		Gui.drawRect(left + f, bottom - f, right, bottom, bcolor);
		Gui.drawRect(right - f, top + f, right, bottom - f, bcolor);
	}
	
}
