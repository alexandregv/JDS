package uk.co.hexeptionclients.jds.keybinds;

import java.util.Arrays;
import java.util.TreeMap;
import java.util.TreeSet;

import uk.co.hexeptionclients.jds.commands.Cmd;

public class KeybindsManager extends TreeMap<String, TreeSet<String>> {
	
	public KeybindsManager() {
		put("B", Cmd.cmdPrefix + "t step");
		put("N", Cmd.cmdPrefix + "t irc");
		put("0", Cmd.cmdPrefix + "t s");
		put("1", Cmd.cmdPrefix + "t x");
		put("3", Cmd.cmdPrefix + "t sd");
		put("4", Cmd.cmdPrefix + "t xd");
		put("5", Cmd.cmdPrefix + "t ss");
		put("6", Cmd.cmdPrefix + "t xa");
		put("7", Cmd.cmdPrefix + "t sg");
		put("8", Cmd.cmdPrefix + "t xt");
	}
	
	public void put(String key, String... commands)
	{
		put(key, new TreeSet<String>(Arrays.asList(commands)));
	}
	
}
