package uk.co.hexeptionclients.jds.gui.keybinds;

import java.io.IOException;
import java.util.TreeSet;
import java.util.Map.Entry;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.keybinds.KeybindsManager;

public class GuiKeybindManager extends GuiScreen {
	
	private GuiScreen lastScreen;
	public static GuiKeybindSlot keybindslot;
	
	public GuiKeybindManager(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}
	
	@Override
	public void initGui() {
		
		keybindslot = new GuiKeybindSlot(mc, this);
		keybindslot.registerScrollButtons(7, 8);
		keybindslot.elementClicked(-1, false, 0, 0);
		
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height - 52, 100, 20, "Add"));
		buttonList.add(new GuiButton(1, width / 2 + 2, height - 52, 100, 20, "Edit"));
		buttonList.add(new GuiButton(2, width / 2 - 100, height - 28, 100, 20, "Remove"));
		buttonList.add(new GuiButton(3, width / 2 + 2, height - 28, 100, 20, "Back"));
		
		buttonList.add(new GuiButton(4, 8, 8, 100, 20, "Reset Keybinds"));
		
		
	}
	
	@Override
	public void updateScreen() {
		((GuiButton) buttonList.get(1)).enabled = keybindslot.getSelectedSlot() != -1;
		((GuiButton) buttonList.get(2)).enabled = keybindslot.getSelectedSlot() != -1;
		super.updateScreen();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		if (button.enabled)
			if (button.id == 0)
				System.out.println(JDS.theClient.keybindsManager.size());
//				mc.displayGuiScreen(new GuiKeybindChange(this, null));
			else if (button.id == 3)
				mc.displayGuiScreen(lastScreen);
			else if (button.id == 4)
				mc.displayGuiScreen(new GuiYesNo(this, "Are you sure you want to reset your keybinds?", "This cannot be undone!", 0));
			else {
				if (keybindslot.getSelectedSlot() > JDS.theClient.keybindsManager.size()){
					keybindslot.elementClicked(JDS.theClient.keybindsManager.size(), false, 0, 0);
				if (button.id == 1) {
					Entry<String, TreeSet<String>> entry = JDS.theClient.keybindsManager.entrySet().toArray(new Entry[JDS.theClient.keybindsManager.size()])[keybindslot.getSelectedSlot()];
					mc.displayGuiScreen(new GuiKeybindChange(this, entry));
				} else if (button.id == 2) {
					Entry<String, String> entry = JDS.theClient.keybindsManager.entrySet().toArray(new Entry[JDS.theClient.keybindsManager.size()])[keybindslot.getSelectedSlot()];
					JDS.theClient.keybindsManager.remove(entry.getKey());
				}
				}}
		
	}
	
	
	
	@Override
	public void confirmClicked(boolean result, int id) {
		if (result) {
			JDS.theClient.keybindsManager = new KeybindsManager();
			// JDS.theClient.fileManager.saveKeybinds();
		}
		mc.displayGuiScreen(this);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		if (keyCode == 28 || keyCode == 156) {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}
	
	@Override
	protected void mouseClicked(int par1, int par2, int par3) throws IOException {
		super.mouseClicked(par1, par2, par3);
		if (par2 >= 36 && par2 <= height - 57)
			if (par1 >= width / 2 + 140 || par1 <= width / 2 - 126)
				keybindslot.elementClicked(-1, false, 0, 0);
	}
	
	@Override
	public void handleKeyboardInput() throws IOException {
		keybindslot.handleMouseInput();
		super.handleKeyboardInput();
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawDefaultBackground();
		keybindslot.drawScreen(mouseX, mouseY, partialTicks);
		drawCenteredString(fontRendererObj, "Keybinds: " + JDS.theClient.keybindsManager.size(), width / 2, 20, 16777215);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
}
