package uk.co.hexeptionclients.jds.commands;

import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.commands.Cmd.CmdInfo;
import uk.co.hexeptionclients.jds.friend.Friend;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

@CmdInfo(help = "Adds or removes players form friends list", name = "friend", syntax = { "[<add> <name>] [<del> <name>] [<list>] [<clear>]" })
public class FriendCmd extends Cmd {
	
	@Override
	public void execute(String[] args) throws Error {
		
		try {
			if (args.length == 0) {
				Wrapper.getInstance().addChatMessage("�cInvalid arguments.");
				Wrapper.getInstance().addChatMessage("�7" + Cmd.cmdPrefix + "friend add �3<name>");
				Wrapper.getInstance().addChatMessage("�7" + Cmd.cmdPrefix + "friend del �3<name>");
				Wrapper.getInstance().addChatMessage("�7" + Cmd.cmdPrefix + "friend list");
				Wrapper.getInstance().addChatMessage("�7" + Cmd.cmdPrefix + "friend clear");
			}
			
			if (args[0].equalsIgnoreCase("add")) {
				String name = args[1];
				if (!JDS.theClient.friendManager.contains(name)) {
					JDS.theClient.friendManager.add(name);
					JDS.theClient.fileManager.saveFriends();
					Wrapper.getInstance().addChatMessage("�7Added �3" + name + "�7 to your friends list.");
				} else {
					Wrapper.getInstance().addChatMessage("�8" + name + "�c is already a friend.");
				}
			}
			
			if (args[0].equalsIgnoreCase("del")) {
				String name = args[1];
				if (JDS.theClient.friendManager.contains(name)) {
					JDS.theClient.friendManager.remove(name);
					JDS.theClient.fileManager.saveFriends();
					Wrapper.getInstance().addChatMessage("�7Removed �3" + name + "�7 from your friend list.");
				} else {
					Wrapper.getInstance().addChatMessage("�8" + name + "�c is not your friend.");
				}
			}
			
			if (args[0].equalsIgnoreCase("list")) {
				Wrapper.getInstance().addChatMessage("�7Friends:");
				
				for (String friend : JDS.theClient.friendManager) {
					if (friend.equals(null)) {
						Wrapper.getInstance().addChatMessage("�3- You Have No Friends :(");
					} else {
						Wrapper.getInstance().addChatMessage("�3- " + friend);
					}
				}
			}
			
			if (args[0].equalsIgnoreCase("clear")) {
				for (String friend : JDS.theClient.friendManager) {
					JDS.theClient.friendManager.clear();
					JDS.theClient.fileManager.saveFriends();
					Wrapper.getInstance().addChatMessage("�7Removed �3all friends �7from friends list.");
				}
			}
		} catch (Exception e) {
		}
		
	}
	
}
