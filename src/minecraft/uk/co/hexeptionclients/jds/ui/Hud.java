package uk.co.hexeptionclients.jds.ui;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

import uk.co.hexeptionclients.jds.events.EventKeyboard;
import uk.co.hexeptionclients.jds.events.EventRender2D;
import uk.co.hexeptionclients.jds.ui.themes.JDSTheme;
import uk.co.hexeptionclients.jds.ui.themes.JDSTheme2;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public class Hud {
	
	private final List<InGameHud> themes = new CopyOnWriteArrayList<InGameHud>();
	private int themeIndex = 0;
	
	public Hud() {
		EventManager.register(this);
	}
	
	public void loadThemes() {
		 themes.add(new JDSTheme());
		 themes.add(new JDSTheme2());
	}
	
	@EventTarget
	public void render2D(EventRender2D event){
		if(Wrapper.getInstance().getGameSettings().showDebugInfo){
			return;
		}
		
		InGameHud currentTheme = getCurrentTheme();
		currentTheme.render(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getMinecraft().displayWidth, Wrapper.getInstance().getMinecraft().displayHeight);
	}
	
	public InGameHud getCurrentTheme() {
		return (InGameHud) this.themes.get(this.themeIndex);
	}
	
	public void onNextTheme() {
		int index = this.themeIndex;
		int maxsize = this.themes.size();
		
		if (index != -1) {
			index++;
			
			if (index >= maxsize) {
				index = 0;
			}
			
			this.themeIndex = index;
		}
	}
	
	@EventTarget
	public void onKeyEvent(EventKeyboard event){
		getCurrentTheme().onKeyPressed(event.key);
	}
	
}
